# bd-livecycle

Machine virtuelle pour intégration continue

## Préparation de la VM

On va récupérer tous les élements sous gitlab.

`git clone git@gitlab.com:swal4u/bd-livecycle.git`

`cd bd-livecycle`

Lancement d'une VM de Continuous Delivery

`vagrant up cd`

Connexion à la VM

`vagrant ssh`

## Récupération du projet

Si première utilisation

`git clone git@gitlab.com:swal4u/bd-dev.git`

puis

`cd bd-dev`

Sinon

`cd bd-dev`

`sudo docker pull`

## Exécution des tests

`sudo docker build -t 10.100.198.200:5000/bd-dev .`

`sudo docker run 10.100.198.200:5000/bd-dev:latest ./run_tests.sh`
